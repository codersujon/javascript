//Log to Console
console.log('I love my Country'); // String print on console
console.log(500); // Number Print on console
console.log(false); //Blooean print on console
console.log(true);

var name = "Awhona Ithi";	//Variable print on console
console.log(name);

console.error("This is Error Message");
console.warn('This is warning Message');

// Array print on console
console.log([
	50, 100, 200,300	
]);
console.log([1, 2, 3,4]);

// Object print on console
console.log({	
	a:500,
	bc:300,
	abc:800
});

console.clear(); // To clear all in the console

//Time Funcion use to print the execution time
console.time('check');
	console.log('Bangladesh');
	console.log('Bangladesh');
	console.log('Bangladesh');
	console.log('Bangladesh');
	console.log('Bangladesh');
	console.log('Bangladesh');
	console.log('Bangladesh');
	console.log('Bangladesh');
console.timeEnd('check');
